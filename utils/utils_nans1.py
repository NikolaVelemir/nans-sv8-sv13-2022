import matplotlib
import math
import matplotlib.pyplot as plt
import seaborn as sb
import pandas as pd
import numpy as np
import statsmodels.api as sm
from statsmodels.regression.linear_model import RegressionResultsWrapper
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split

matplotlib.rcParams['figure.figsize'] = (8, 4)
sb.set(font_scale=1.)


'''
Function that finds the best combination of features by dropping each of the columns in x_train
If the model with dropped feature has smaller mse than original model we assign it as best model
param model: input model
param df: input dataframe
param get_model: function that fits one of the models
param get_error: function that calculates error for corresponding model
return best model, a column that got dropped in order to achieve best model, dataframe without that column
'''
def drop_column(model, df: pd.DataFrame, get_model, get_error):
    x_train, x_val, x_test, y_train, y_val, y_test = divide_data(df)
    smallest_error = get_error(model, x_val, y_val)
    best_model = model
    best_df = df
    dropped = None
    column_names = x_train.columns.tolist()
    for column in column_names:
        new_df = df.drop(columns=[column])
        model = get_model(new_df)
        x_train_new, x_val_new, x_test_new, y_train_new, y_val_new, y_test_new = divide_data(new_df)
        curr_error = get_error(model, x_val_new, y_val_new)
        if curr_error < smallest_error:
            smallest_error = curr_error
            best_model = model
            dropped = column
            best_df = new_df

    return best_model, dropped, best_df


'''
Function that calls drop column function until there is no more columns to drop or 
Until dropped_column is None which means there is no better combination of features
param model: input model
param df: input dataframe
param get_model: function that fits one of the models
param get_error: function that calculates error for corresponding model
return best fitted model and list of columns we dropped
'''
def get_best_metric(model, df: pd.DataFrame, get_model, get_error):
    best_model = model
    best_df = df
    best_dropped = []
    for i in range(len(df)-3):
        new_model, new_dropped, new_df = drop_column(best_model, best_df, get_model, get_error)
        if not new_dropped:
            break
        best_dropped.append(new_dropped)
        best_model = new_model
        best_df = new_df
    return best_model, best_dropped


'''
Function that calls appropriated function for merging train and validation data in order to train model with more data
param df: dataframe
param dropped: list of columns we dropped in order to achieve best combination of features
fit_model_merge: function that fits corresponding model
return fitted model and mean squared error on test data
'''
def merge_train_val(df: pd.DataFrame, dropped, fit_model_merge):
    df = df.drop(columns=dropped)
    model, error = fit_model_merge(df)
    return model, error

'''
Function that divides dataframe into train/val/test in 60/20/20 ratio
param df: dataframe
return train, validation and test features and labels
'''
def divide_data(df: pd.DataFrame):
    x = df.drop(columns=['Price.DE.', 'PriceLog'])
    y = df['PriceLog']

    x_train, x_rest, y_train, y_rest = train_test_split(x, y, train_size=0.6, shuffle=True, random_state=42)
    x_val, x_test, y_val, y_test = train_test_split(x_rest, y_rest, train_size=0.5, shuffle=True, random_state=42)

    return x_train, x_val, x_test, y_train, y_val, y_test

'''
Function that 'cleans up' dataset 
It removes all unnecessary columns and fills up missing values using spline
it also creates our new target variable 'PriceLog' by loging 'Price.DE.' column
It helps with L.I.N.E assumptions
param df: dataframe
return: filtered dataframe
'''
def get_dataset(df: pd.DataFrame):
    df = df.drop(columns=['Car_name', 'Car_name_link'])
    df = df.dropna()
    df = df[(df['Price.DE.'] <= 0) == False]
    df['PriceLog'] = df['Price.DE.'].apply(lambda x: math.log(x))

    return df


def calculate_residuals(model, features, labels):
    '''Calculates residuals between true value `labels` and predicted value.'''
    y_pred = model.predict(features)
    df_results = pd.DataFrame({'Actual': labels, 'Predicted': y_pred})
    df_results['Residuals'] = abs(df_results['Actual']) - abs(df_results['Predicted'])
    return df_results


def linear_assumption(model: LinearRegression | RegressionResultsWrapper, features: np.ndarray | pd.DataFrame,
                      labels: pd.Series, p_value_thresh=0.05, plot=True):
    '''
    Linear assumption: assumees linear relation between the independent and dependent variables to be linear.
    Testing linearity using the F-test.

    Interpretation of `p-value`:
    - `p-value >= p_value_thresh` indicates linearity between.
    - `p-value < p_value_thresh` doesn't indicate linearity.

    Returns (only if the model is from `statsmodels` not from `scikit-learn`):
    - is_linearity_found: A boolean indicating whether the linearity assumption is supported by the data.
    - p_value: The p-value obtained from the linearity test.
    '''
    df_results = calculate_residuals(model, features, labels)
    y_pred = df_results['Predicted']

    if plot:
        plt.figure(figsize=(6, 6))
        plt.scatter(labels, y_pred, alpha=.5)
        # x = y line
        line_coords = np.linspace(np.concatenate([labels, y_pred]).min(), np.concatenate([labels, y_pred]).max())
        plt.plot(line_coords, line_coords, color='darkorange', linestyle='--')
        plt.title('Linear assumption')
        plt.xlabel('Actual')
        plt.ylabel('Predicted')
        plt.show()

    if type(model) == RegressionResultsWrapper:
        p_value = model.f_pvalue
        is_linearity_found = True if p_value < p_value_thresh else False
        return is_linearity_found, p_value
    else:
        pass


def independence_of_errors_assumption(model, features, labels, plot=True):
    '''
    Independence of errors: assumes independent errors. 
    Assumes that there is no autocorrelation in the residuals. 
    Testing autocorrelation using Durbin-Watson Test.
    
    Interpretation of `d` value:
    - 1.5 <= d <= 2: No autocorrelation (independent residuals).
    - d < 1.5: Positive autocorrelation.
    - d > 2: Negative autocorrelation.

    Returns:
    - autocorrelation: The type of autocorrelation ('positive', 'negative', or None).
    - dw_value: The Durbin-Watson statistic value.
    '''
    df_results = calculate_residuals(model, features, labels)

    if plot:
        sb.scatterplot(x='Predicted', y='Residuals', data=df_results)
        plt.axhline(y=0, color='darkorange', linestyle='--')
        plt.show()

    from statsmodels.stats.stattools import durbin_watson
    dw_value = durbin_watson(df_results['Residuals'])

    autocorrelation = None
    if dw_value < 1.5:
        autocorrelation = 'positive'
    elif dw_value > 2:
        autocorrelation = 'negative'
    else:
        autocorrelation = None
    return autocorrelation, dw_value


def normality_of_errors_assumption(model, features, label, p_value_thresh=0.05, plot=True):
    '''
    Normality of errors: assumes normally distributed residuals around zero.
    Testing using the Anderson-Darling test for normal distribution on residuals.
    Interpretation of `p-value`:
    - `p-value >= p_value_thresh` indicates normal distribution.
    - `p-value < p_value_thresh` indicates non-normal distribution.

    Returns:
    - dist_type: A string indicating the distribution type ('normal' or 'non-normal').
    - p_value: The p-value from the Anderson-Darling test.
    '''
    df_results = calculate_residuals(model, features, label)

    if plot:
        plt.title('Distribution of residuals')
        sb.histplot(df_results['Residuals'], kde=True, kde_kws={'cut': 3})
        plt.show()

    from statsmodels.stats.diagnostic import normal_ad
    p_value = normal_ad(df_results['Residuals'])[1]
    dist_type = 'normal' if p_value >= p_value_thresh else 'non-normal'
    return dist_type, p_value


def equal_variance_assumption(model, features, labels, p_value_thresh=0.05, plot=True):
    '''
    Equal variance: assumes that residuals have equal variance across the regression line.
    Testing equal variance using Goldfeld-Quandt test.
    
    Interpretation of `p-value`:
    - `p-value >= p_value_thresh` indicates equal variance.
    - `p-value < p_value_thresh` indicates non-equal variance.

    Returns:
    - dist_type: A string indicating the distribution type ('eqal' or 'non-eqal').
    - p_value: The p-value from the Goldfeld-Quandt test.
    '''
    df_results = calculate_residuals(model, features, labels)

    if plot:
        sb.scatterplot(x='Predicted', y='Residuals', data=df_results)
        plt.axhline(y=0, color='darkorange', linestyle='--')
        plt.show()

    if type(model) == LinearRegression:
        features = sm.add_constant(features)
    p_value = sm.stats.het_goldfeldquandt(df_results['Residuals'], features)[1]
    dist_type = 'equal' if p_value >= p_value_thresh else 'non-equal'
    return dist_type, p_value


def perfect_collinearity_assumption(features: pd.DataFrame, plot=True):
    '''
    Perfect collinearity: assumes no perfect correlation between two or more features.
    Testing perfect collinearity between exactly two features using correlation matrix.

    Returns:
    - `has_perfect_collinearity`: A boolean indicating if perfect collinearity was found.
    '''
    correlation_matrix = features.corr()  # racunamo matricu korelacije

    if plot:
        sb.heatmap(correlation_matrix, annot=True, cmap='coolwarm', fmt='.2f', linewidths=0.1)
        plt.title('Matrica korelacije')
        plt.show()

    np.fill_diagonal(correlation_matrix.values, np.nan)
    pos_perfect_collinearity = (correlation_matrix > 0.999).any().any()
    neg_perfect_collinearity = (correlation_matrix < -0.999).any().any()
    has_perfect_collinearity = pos_perfect_collinearity or neg_perfect_collinearity
    return has_perfect_collinearity


def are_assumptions_satisfied(model, features, labels, p_value_thresh=0.05):
    '''Check if all assumptions in multiple linear regression are satisfied.'''
    x_with_const = sm.add_constant(features)
    is_linearity_found, p_value = linear_assumption(model, x_with_const, labels, p_value_thresh, plot=False)
    print(f'Linearnost :{is_linearity_found}')
    autocorrelation, dw_value = independence_of_errors_assumption(model, x_with_const, labels, plot=False)
    print(f'Autokoleracija :{autocorrelation}')
    n_dist_type, p_value = normality_of_errors_assumption(model, x_with_const, labels, p_value_thresh, plot=False)
    print(f'Nomrlanost :{n_dist_type}')
    e_dist_type, p_value = equal_variance_assumption(model, x_with_const, labels, p_value_thresh, plot=False)
    print(f'Jednaka varijansa :{e_dist_type}')
    has_perfect_collinearity = perfect_collinearity_assumption(features, plot=False)
    print(f'Kolinearnost:{has_perfect_collinearity}')

    if is_linearity_found and autocorrelation is None and n_dist_type == 'normal' and e_dist_type == 'equal' and not has_perfect_collinearity:
        return True
    else:
        return False


def get_fitted_model(features, labels):
    '''Fits the model usting `statsmodels` package.'''
    x_with_const = sm.add_constant(features, has_constant='add')
    model = sm.OLS(labels, x_with_const).fit()
    return model


def get_mse(model, features, labels):
    y_pred = model.predict(sm.add_constant(features, has_constant='add'))
    s = (np.exp(labels) - np.exp(y_pred))
    mse = np.mean(s ** 2)
    return mse

