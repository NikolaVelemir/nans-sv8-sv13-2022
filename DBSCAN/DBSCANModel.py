import pandas as pd
from utils import utils_nans1 as un
from utilsDBSCAN import fitDBSCAN

df = pd.read_csv('../data/original.csv')
df = un.get_dataset(df)

model, mse, dropped = fitDBSCAN(df)
print(mse)  # 62597707.79
print(dropped)  # []
