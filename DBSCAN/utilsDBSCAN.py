import math

from sklearn.cluster import DBSCAN
from utils import utils_nans1 as un
import numpy as np
import pandas as pd

'''
Function that finds best combination of features and fits a model of linear regression using dbscan
Also finds the best combination for parameters eps and min_samples
eps - maximum distance between two points for them to be considered as neighbours
min_samples - minimum number of neighbours for them to be considered as cluster
param df: dataframe
return: fitted model, mse and dropped features for acquiring best model
'''
def fitDBSCAN(df: pd.DataFrame):
    best_model = None
    best_mse = math.inf
    best_dropped = []
    epsilons = [i for i in range(70, 100, 2)]
    samples = [i for i in range(10, 30, 2)]

    for eps in epsilons:
        for min_sample in samples:
            x = df.drop(columns=['Price.DE.', 'PriceLog'])
            y = df['PriceLog']

            dbscan = DBSCAN(eps=float(eps), min_samples=min_sample)
            clusters = dbscan.fit_predict(x)
            mask = clusters != -1
            clustered_df = df[mask]

            model = get_model_dbscan(clustered_df)
            _, dropped = un.get_best_metric(model, clustered_df, get_model_dbscan, get_error_dbscan)
            model, test_mse = un.merge_train_val(clustered_df, dropped, get_model_merge)

            if test_mse < best_mse:
                best_mse = test_mse
                best_model = model
                best_dropped = dropped

    return best_model, best_mse, best_dropped


'''
Fits dbscan model using OLS
param df: dataframe
return: fitted model
'''
def get_model_dbscan(df):
    x = df.drop(columns=['Price.DE.', 'PriceLog'])
    y = df['PriceLog']
    x_train, _, y_train, _ = un.train_test_split(x, y, train_size=0.6, shuffle=True, random_state=42)
    model = un.get_fitted_model(x_train, y_train)
    return model


'''
Function that fits the model using the best combination of features and by combining test and validation data
param df: dataframe
return: model and mean squared error calculated on test data
'''
def get_model_merge(df):
    x_train, x_val, x_test, y_train, y_val, y_test = un.divide_data(df)
    x_concat = pd.concat([x_train, x_val])
    y_concat = pd.concat([y_train, y_val])
    model = un.get_fitted_model(x_concat, y_concat)
    test_mse = get_error_dbscan(model, x_test, y_test)
    return model, test_mse


'''
Returns mean squared error(mse) for our model and validation/test data
param model: fitted model
param x: features
param y: labels
return: mean squared error
'''
def get_error_dbscan(model, x, y):
    return un.get_mse(model, x, y)


