import pandas as pd
import numpy as np
from sklearn.linear_model import RANSACRegressor
from sklearn.metrics import mean_squared_error
import utils.utils_nans1 as un

'''
Function that fits the model using ransac - random sample consensus
param df: dataframe
return: model, test mean squared error
'''
def fitRANSAC(df: pd.DataFrame):
    x = df.drop(columns=['Price.DE.', 'PriceLog'])
    y = df['PriceLog']
    assert len(x) == len(y)
    n = len(x)
    x_train, x_test, y_train, y_test = un.train_test_split(x, y, train_size=0.8, shuffle=True,
                                                           random_state=42)
    ransac = RANSACRegressor(residual_threshold=2)
    ransac.fit(x_train, y_train)
    predictions = ransac.predict(x_test)
    mse = mean_squared_error(np.exp(y_test), np.exp(predictions))
    return ransac, mse


