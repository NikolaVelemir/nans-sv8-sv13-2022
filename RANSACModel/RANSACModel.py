import pandas as pd
from utils import utils_nans1 as un
from utilsRANSAC import fitRANSAC

df = pd.read_csv('../data/original.csv')
df = un.get_dataset(df)

model, mse = fitRANSAC(df)
print(mse)  # 151182056.43

