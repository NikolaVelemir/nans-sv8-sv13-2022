import pandas as pd
from utilsTLS import fitTLS
from utils import utils_nans1 as un

df = pd.read_csv('../data/original.csv')
df = un.get_dataset(df)


model, mse, dropped = fitTLS(df)
print(mse)  # 375598194.57
print(dropped)  # ['Fast_charge', 'acceleration..0.100.']