import utils.utils_nans1 as un
from sklearn.linear_model import TheilSenRegressor
import pandas as pd
import numpy as np
import statsmodels.api as sm

'''
Function that fits simple linear regression model for each feature in our dataframe
Then we calculated predicted values for each of the models and use that predicted values as features for our new dataframe
param df: dataframe
return dataframe with new features
'''
def get_new_features(df: pd.DataFrame):
    column_names = df.columns.tolist()
    models = {}
    y = df['PriceLog']
    for name in column_names:
        if name == 'Price.DE.' or name == 'PriceLog':
            continue
        vector = df[name]
        x = np.array(vector).reshape(-1, 1)
        model = TheilSenRegressor().fit(x, y)
        slope, intercept = model.coef_[0], model.intercept_
        models[name] = (slope, intercept)

    df_predictions = pd.DataFrame()
    for k in models:
        vector = df[k]
        x = np.array(vector).reshape(-1, 1)
        predictions = x * models[k][0] + models[k][1]
        df_predictions[k] = pd.Series(predictions.flatten())

    df_predictions['PriceLog'] = y
    df_predictions['Price.DE.'] = df['Price.DE.']
    df_predictions.replace([np.inf, -np.inf], np.nan, inplace=True)
    df_predictions = df_predictions.dropna()

    return df_predictions

'''
Function that finds best combination of features and fits the model using modified theil sen estimator
param df: dataframe
return: fitted model, mse and dropped features for acquiring best model
'''
def fitTLS(df: pd.DataFrame):
    df_predictions = get_new_features(df)
    model = get_model_tls(df_predictions)
    _, dropped = un.get_best_metric(model, df_predictions, get_model_tls, get_error_tls)
    best_model, test_mse = un.merge_train_val(df_predictions, dropped, get_model_merge)
    return best_model, test_mse, dropped

'''
Function that fits model using modified theil sen estimator
param df: dataframe
return: fitted model
'''
def get_model_tls(df):
    x_train, _, _, y_train, _, _ = un.divide_data(df)
    model = un.get_fitted_model(x_train, y_train)
    return model

'''
Function that fits the model using the best combination of features and by combining test and validation data
param df: dataframe
return: model and mean squared error calculated on test data
'''
def get_model_merge(df):
    x_train, x_val, x_test, y_train, y_val, y_test = un.divide_data(df)
    x_concat = pd.concat([x_train, x_val])
    y_concat = pd.concat([y_train, y_val])
    model = un.get_fitted_model(x_concat, y_concat)
    test_mse = get_error_tls(model, x_test, y_test)
    return model, test_mse

'''
Returns mean squared error(mse) for our model and validation/test data
param model: fitted model
param x: features
param y: labels
return: mean squared error
'''
def get_error_tls(model, x, y):
    y_pred = model.predict(sm.add_constant(x))
    s = (np.exp(y) - np.exp(y_pred))
    mse = np.mean(s ** 2)
    return mse

