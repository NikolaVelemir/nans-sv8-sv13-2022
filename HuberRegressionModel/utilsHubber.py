import numpy as np
import pandas as pd
from sklearn.linear_model import HuberRegressor

import utils.utils_nans1 as un

'''
Function that finds best combination of features and fits the model
param: df
return: fitted model, mse and dropped features for acquiring best model
'''
def fitHuber(df: pd.DataFrame):
    x_train, x_val, x_test, y_train, y_val, y_test = un.divide_data(df)
    model = HuberRegressor(max_iter=1000).fit(x_train, y_train)
    _, dropped = un.get_best_metric(model, df, get_model_huber, get_error_huber)
    model, test_mse = un.merge_train_val(df, dropped, get_model_merge)
    return model, test_mse, dropped

'''
Function that fits model using HUberRegressor
param df: dataframe
return: model
'''
def get_model_huber(df):
    x_train, _, _, y_train, _, _ = un.divide_data(df)
    model = HuberRegressor(max_iter=1000).fit(x_train, y_train)
    return model

'''
Function that fits the model using the best combination of features and by combining test and validation data
param df: dataframe
return: model and mean squared error calculated on test data
'''
def get_model_merge(df):
    x_train, x_val, x_test, y_train, y_val, y_test = un.divide_data(df)
    x_concat = pd.concat([x_train, x_val])
    y_concat = pd.concat([y_train, y_val])
    model = HuberRegressor(max_iter=1000).fit(x_concat, y_concat)
    test_mse = get_error_huber(model, x_test, y_test)
    return model, test_mse


'''
Returns mean squared error(mse) for our model and validation/test data
param model: fitted model
param x: features
param y: labels
return: mean squared error
'''
def get_error_huber(model, x, y):
    y_pred = model.predict(x)
    y_true_exp = np.exp(y)
    y_pred_exp = np.exp(y_pred)
    y_true_exp_en = [(index, value) for index, value in enumerate(y_true_exp)]
    y_pred_exp_en = [(index, value) for index, value in enumerate(y_pred_exp)]

    mse = np.mean(((np.exp(y) - np.exp(y_pred)) ** 2))
    return mse

