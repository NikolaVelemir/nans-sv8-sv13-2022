import pandas as pd
from utils import utils_nans1 as un
from utilsHubber import fitHuber

df = pd.read_csv('../data/original.csv')
df = un.get_dataset(df)

model, mse, dropped = fitHuber(df)
print(mse)  # 412609017.08
print(dropped)  # ['Battery', 'Range', 'acceleration..0.100.']
