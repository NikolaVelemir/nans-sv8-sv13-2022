import pandas as pd

import utils.utils_nans1 as un

'''
Function that finds best combination of features and fits the model using ordinary least squares
param df: dataframe
return: fitted model, mse and dropped features for acquiring best model
'''
def fitOLS(df: pd.DataFrame):
    x_train, x_val, x_test, y_train, y_val, y_test = un.divide_data(df)
    model = un.get_fitted_model(x_train, y_train)
    _, dropped = un.get_best_metric(model, df, get_model_ols, get_error_ols)
    best_model, test_mse = un.merge_train_val(df, dropped, get_model_merge)
    return best_model, test_mse, dropped

'''
Function that returns model fitted using ordinary least squares
'''
def get_model_ols(df):
    x_train, _, _, y_train, _, _ = un.divide_data(df)
    model = un.get_fitted_model(x_train, y_train)
    return model

'''
Function that fits the model using the best combination of features and by combining test and validation data
param df: dataframe
return: model and mean squared error calculated on test data
'''
def get_model_merge(df):
    x_train, x_val, x_test, y_train, y_val, y_test = un.divide_data(df)
    x_concat = pd.concat([x_train, x_val])
    y_concat = pd.concat([y_train, y_val])
    model = un.get_fitted_model(x_concat, y_concat)
    test_mse = get_error_ols(model, x_test, y_test)
    return model, test_mse

'''
Returns mean squared error(mse) for our model and validation/test data
param model: fitted model
param x: features
param y: labels
return: mean squared error
'''
def get_error_ols(model, x, y):
    return un.get_mse(model, x, y)




