import pandas as pd
from utils import utils_nans1 as un
from utilsOLS import fitOLS

df = pd.read_csv('../data/original.csv')
df = un.get_dataset(df)

model, mse, dropped = fitOLS(df)
print(mse)  # 397336542.97
print(dropped)  # ['acceleration..0.100.', 'Battery', 'Range']





