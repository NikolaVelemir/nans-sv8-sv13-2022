import pandas as pd
from utils import utils_nans1 as un
from utils.utils_nans1 import get_dataset
from utilsKNN import fitKNN

df = pd.read_csv('../data/original.csv')
df = un.get_dataset(df)

model, mse, dropped = fitKNN(df)
print(mse)  # 11796424.62
print(dropped)  # ['Range']
