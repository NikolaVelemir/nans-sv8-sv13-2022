import pandas as pd
import numpy as np
from sklearn.neighbors import KNeighborsRegressor
import utils.utils_nans1 as un


'''
Function that finds best combination of features and fits the model using KNN
param df: dataframe
return: fitted model, mse and dropped features for acquiring best model
'''
def fitKNN(df: pd.DataFrame):
    knn = get_model_knn(df)
    _, dropped = un.get_best_metric(knn, df, get_model_knn, get_error_knn)
    model, test_mse = un.merge_train_val(df, dropped, get_model_merge)
    return model, test_mse, dropped

'''
Function that fits model using KNN model
param df: dataframe
return: fitted model
'''
def get_model_knn(df: pd.DataFrame):
    x_train, _, _, y_train, _, _ = un.divide_data(df)
    knn = KNeighborsRegressor(n_neighbors=5).fit(x_train, y_train)
    return knn

'''
Function that fits the model using the best combination of features and by combining test and validation data
param df: dataframe
return: model and mean squared error calculated on test data
'''
def get_model_merge(df):
    x_train, x_val, x_test, y_train, y_val, y_test = un.divide_data(df)
    x_concat = pd.concat([x_train, x_val])
    y_concat = pd.concat([y_train, y_val])
    knn = KNeighborsRegressor(n_neighbors=5).fit(x_concat, y_concat)
    test_mse = get_error_knn(knn, x_test, y_test)
    return knn, test_mse

'''
Returns mean squared error(mse) for our model and validation/test data
param model: fitted model
param x: features
param y: labels
return: mean squared error
'''
def get_error_knn(model, x, y):
    pred = model.predict(x)
    error = np.mean(np.exp(pred) - np.exp(y))**2
    return error




