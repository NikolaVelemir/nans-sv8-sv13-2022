import pandas as pd
import statsmodels.api as sm
import numpy as np

import utils.utils_nans1 as un


'''
Function that finds best combination of features and fits the model using weighted least squares
param df: dataframe
return: fitted model, mse and dropped features for acquiring best model
'''
def fitWLS(df: pd.DataFrame):
    wls_model = get_model_wls(df)
    _, dropped = un.get_best_metric(wls_model, df, get_model_wls, get_error_wls)
    best_model, test_mse = un.merge_train_val(df, dropped, get_model_merge)
    return best_model, test_mse, dropped


'''
Function that fits model using weighted least squares
First we fit the model using ols then we set our outlier condition based on that model
Then we use that outlier condition for assigning weights to our residuals
param df: dataframe
param flag: if set to true divides dataframe into train/val/test, else divides dataframe into train/test
so that train and val data are combined in order to calculate test mse for best combination of features
return fitted model
'''
def get_model_wls(df: pd.DataFrame, flag=True):
    if flag:
        x_train, _, _, y_train, _, _ = un.divide_data(df)
    else:
        x_train, x_val, _, y_train, y_val, _ = un.divide_data(df)
        x_train = pd.concat([x_train, x_val])
        y_train = pd.concat([y_train, y_val])
    model = un.get_fitted_model(x_train, y_train)

    residuals = model.resid
    weights = 1 / np.exp(residuals)
    wls_model = sm.WLS(y_train, sm.add_constant(x_train), weights=weights).fit()
    return wls_model


'''
Function that fits the model using the best combination of features and by combining test and validation data
param df: dataframe
return: model and mean squared error calculated on test data
'''


def get_model_merge(df: pd.DataFrame):
    _, _, x_test, _, _, y_test = un.divide_data(df)
    wls_model = get_model_wls(df, flag=False)
    test_mse = get_error_wls(wls_model, x_test, y_test)

    return wls_model, test_mse


'''
Returns mean squared error(mse) for our model and validation/test data
param model: fitted model
param x: features
param y: labels
return: mean squared error
'''


def get_error_wls(model, x, y):
    return un.get_mse(model, x, y)
