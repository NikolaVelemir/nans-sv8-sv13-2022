import pandas as pd
from utils import utils_nans1 as un
from utilsWLS import fitWLS

df = pd.read_csv('../data/original.csv')
df = un.get_dataset(df)

model, mse, dropped = fitWLS(df)

print(mse)  # 388868515.13
print(dropped)  # ['acceleration..0.100.']
