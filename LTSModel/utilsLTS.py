import math
import pandas as pd

import utils.utils_nans1 as un

'''
Function that finds best combination of features and fits the model using least trimmed squares
param df: dataframe
return: fitted model, mse and dropped features for acquiring best model
'''
def fitLTS(df: pd.DataFrame):
    minimal_model = get_model_lts(df)

    _, dropped = un.get_best_metric(minimal_model, df, get_model_lts, get_error_lts)
    model, test_mse = un.merge_train_val(df, dropped, get_model_merge)
    return model, test_mse, dropped


'''
Function that fits model using least trimmed squares
param df: dataframe
param num_observations: number of different subsets of length h
param flag: if set to true divides dataframe into train/val/test, else divides dataframe into train/test
so that train and val data are combined in order to calculate test mse for best combination of features
return: fitted model
'''


def get_model_lts(df, num_observations=300, flag=True):
    n = len(df)

    h = int(math.floor(n / 2 + (num_observations + 1) / 2)) if int(
        math.floor(n / 2 + (num_observations + 1) / 2)) < n else n

    minimal_model = None
    for k in range(num_observations):
        df_sampled = df.copy().sample(h)
        if flag:
            x_train, x_val, x_test, y_train, y_val, y_test = un.divide_data(df_sampled)
        else:
            x_train, x_val_train, x_val, y_train, y_val_train, y_val = un.divide_data(df)
            x_train = pd.concat([x_train, x_val_train])
            y_train = pd.concat([y_train, y_val_train])

        model = [un.get_fitted_model(x_train, y_train), x_val, y_val]

        if not minimal_model:
            minimal_model = model
        else:
            if un.get_mse(model[0], model[1], model[2]) < un.get_mse(minimal_model[0], minimal_model[1],
                                                                     minimal_model[2]):
                minimal_model = model

    return minimal_model[0]


'''
Function that fits the model using the best combination of features and by combining test and validation data
param df: dataframe
return: model and mean squared error calculated on test data
'''


def get_model_merge(df):
    _, _, x_test, _, _, y_test = un.divide_data(df)
    model = get_model_lts(df, flag=False)
    test_mse = un.get_mse(model, x_test, y_test)
    return model, test_mse


'''
Returns mean squared error(mse) for our model and validation/test data
param model: fitted model
param x: features
param y: labels
return: mean squared error
'''


def get_error_lts(model, x, y):
    return un.get_mse(model, x, y)
