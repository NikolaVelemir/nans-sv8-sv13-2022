import pandas as pd
from utils import utils_nans1 as un
from utilsLTS import fitLTS

df = pd.read_csv('../data/original.csv')
df = un.get_dataset(df)

model, mse, dropped = fitLTS(df)
print(mse)  # 368504717.27
print(dropped)  # ['Range']
