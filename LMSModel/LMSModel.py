import pandas as pd
from utils import utils_nans1 as un
from utilsLMS import fitLMS

df = pd.read_csv('../data/original.csv')
df = un.get_dataset(df)

model, mse, dropped = fitLMS(df)

print(mse)  # 339535638.37
print(dropped)  # ['Fast_charge']
