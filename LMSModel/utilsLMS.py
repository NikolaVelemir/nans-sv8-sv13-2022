import pandas as pd
import math
import utils.utils_nans1 as un
import numpy as np
import statsmodels.api as sm


'''
Function that finds best combination of features and fits the model using least median squares
param df: dataframe
return: fitted model, mse and dropped features for acquiring best model
'''
def fitLMS(df: pd.DataFrame):
    best_model = get_model_lms(df, flag=True)

    _, dropped = un.get_best_metric(best_model, df, get_model_lms, get_error_lms)
    new_model, test_mse = un.merge_train_val(df, dropped, get_model_merge)
    return new_model, test_mse, dropped

'''
Function that returns least median squares model
param df: dataframe
param num_observations: number of different subsets
param flag: if set to true divides dataframe into train/val/test, else divides dataframe into train/test
so that train and val data are combined in order to calculate test mse for best combination of features 
return: fitted model
'''
def get_model_lms(df, num_observations=50, flag=True):
    n = len(df)

    min_median_residual = float('inf')
    best_model = None
    h = int(math.floor(n / 2 + (num_observations + 1) / 2)) if int(math.floor(n / 2 + (num_observations + 1) / 2)) < n else n

    for _ in range(num_observations):
        df_sampled = df.copy().sample(h)
        if flag:
            x_train, x_val, x_test, y_train, y_val, y_test = un.divide_data(df_sampled)
        else:
            x_train, x_val_train, x_val, y_train, y_val_train, y_val = un.divide_data(df)
            x_train = pd.concat([x_train, x_val_train])
            y_train = pd.concat([y_train, y_val_train])

        model = un.get_fitted_model(x_train, y_train)
        predictions = model.predict(sm.add_constant(x_val))
        residuals = np.abs(y_val - predictions)
        median_residual = np.median(residuals)

        if median_residual < min_median_residual:
            min_median_residual = median_residual
            best_model = model

    return best_model


'''
Function that fits the model using the best combination of features and by combining test and validation data
param df: dataframe
return: model and mean squared error calculated on test data
'''
def get_model_merge(df):
    _, _, x_test, _, _, y_test = un.divide_data(df)
    model = get_model_lms(df, flag=False)
    test_mse = un.get_mse(model, x_test, y_test)
    return model, test_mse


'''
Returns mean squared error(mse) for our model and validation/test data
param model: fitted model
param x: features
param y: labels
return: mean squared error
'''
def get_error_lms(model, x_val, y_val):
    predictions = model.predict(sm.add_constant(x_val))
    residuals = np.abs(y_val - predictions)
    median_residual = np.median(residuals)
    return median_residual
